$sfmlURL64 = "https://www.sfml-dev.org/files/SFML-2.5.1-windows-vc15-64-bit.zip"
$sfmlOutput = "$PSScriptRoot\sfml-tmp.zip"
$start_time = Get-Date

Write-Output "SFML 64 downloading..."

Invoke-WebRequest -Uri $sfmlURL64 -OutFile $sfmlOutput
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"

Write-Output "SFML 64 opening zip..."

Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

Unzip "$PSScriptRoot\sfml-tmp.zip" "$PSScriptRoot\SFML"

Write-Output "SFML 64 successefully downloaded"

Write-Output "Removing tmp files ..."

Remove-Item -path "$PSScriptRoot\sfml-tmp.zip"
