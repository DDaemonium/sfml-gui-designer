#include "../window/BaseWindow.hpp"
#include "Application.hpp"

namespace GUI
{

	void Application::Run()
	{
		sf::Event currentEvent;
		std::unique_lock lg(poolLock, std::defer_lock);
		while (!windowsPool.empty())
		{
			for (const auto& windowPtr : windowsPool)
			{
				// if window already deleted or closed
				if (!windowPtr->GetPtr())
				{
					lg.lock();
					windowsPool.erase(windowPtr);
					lg.unlock();
					break;
				}

				bool isClosed = false;
				while (windowPtr->GetPtr() && windowPtr->GetPtr()->pollEvent(currentEvent))
				{
					auto window = windowPtr->GetPtr();
					if (currentEvent.type == sf::Event::Closed)
					{
						window->close();
						isClosed = true;
						lg.lock();
						windowsPool.erase(windowPtr);
						lg.unlock();
						break;
					}
					else if (currentEvent.type == sf::Event::KeyPressed)
					{
						window->NotifyControlsKeyPressedEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::Resized)
					{
						window->NotifyControlsResizedEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::LostFocus)
					{
						window->NotifyControlsLostFocusEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::GainedFocus)
					{
						window->NotifyControlsGainedFocusEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::TextEntered)
					{
						window->NotifyControlsTextEnteredEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::MouseWheelScrolled)
					{
						window->NotifyControlsMouseWheelScrolledEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::MouseButtonPressed)
					{
						window->NotifyControlsMouseButtonPressedEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::MouseMoved)
					{
						window->NotifyControlsMouseMovedEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::MouseEntered)
					{
						window->NotifyControlsMouseEnteredEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::MouseLeft)
					{
						window->NotifyControlsMouseLeftEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::JoystickButtonPressed)
					{
						window->NotifyControlsJoystickButtonPressedEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::JoystickMoved)
					{
						window->NotifyControlsJoystickMovedEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::JoystickConnected)
					{
						window->NotifyControlsJoystickConnectedEvent(currentEvent);
					}
					else if (currentEvent.type == sf::Event::JoystickDisconnected)
					{
						window->NotifyControlsJoystickDisconnectedEvent(currentEvent);
					}
				}
				if (!isClosed)
				{
					windowPtr->GetPtr()->display();
				}
				else
				{
					break;
				}
			}
		}
	}

	void Application::AddWindow(std::unique_ptr<BaseWindowPtr> window)
	{
		std::lock_guard lg(poolLock);
		windowsPool.insert(std::move(window));
	}

}
