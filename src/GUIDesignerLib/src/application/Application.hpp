#pragma once

#include "../window/BaseWindowPtr.hpp"
#include <memory>
#include <unordered_set>
#include <mutex>

namespace GUI
{

	class BaseWindowPtr;
	class Application final
	{
	private:
		friend BaseWindow;
		Application() = default;
		void AddWindow(std::unique_ptr<BaseWindowPtr> window);
		Application(const Application&) = delete;
		Application& operator=(const Application&) = delete;
		Application(Application&&) = delete;
		Application& operator=(Application&&) = delete;
		
	public:
		~Application() = default;
		static auto& Instance()
		{
			static Application instance;
			return instance;
		}

		void Run();

	public:
		std::unordered_set<std::unique_ptr<BaseWindowPtr>> windowsPool;
		mutable std::mutex poolLock;
	};
}