#include "Button.hpp"

namespace Controls {
    void DependencyProperty::SetValue(const bool& value)
    {
        m_Value = value;
    }

    bool DependencyProperty::GetValue() const
    {
        return m_Value;
    }

    Button::Button()
    {
    }

    Button::~Button()
    {
    }

    void Button::SetCancel(const bool& cancelProperty)
    {
        m_IsCancelProperty.SetValue(cancelProperty);
    }

    bool Button::GetCancel() const
    {
        return m_IsCancelProperty.GetValue();
    }

    void Button::SetDefaultProperty(const bool& defaultProperty)
    {
        m_IsDefaultProperty.SetValue(defaultProperty);
    }

    bool Button::GetDefaultProperty() const
    {
        return m_IsDefaultProperty.GetValue();
    }

    bool Button::GetDefaultedProperty() const
    {
        return m_IsDefaultedProperty.GetValue();
    }

    AutomationPeer Button::OnCreateAutomationPeer() const
    {
        return AutomationPeer();
    }
} // namespace Controls