#pragma once

#include "Control.hpp"

namespace Controls {
    // AutomationPeer is declared into Label.hpp
    typedef AutomationPeer;

    class DependencyProperty {
    public:
        DependencyProperty();

        virtual bool GetValue() const;
        virtual void SetValue(const bool&);

    private:
        bool         m_Value;
    };

    class Button : public Control {
    public:
        Button();
        virtual ~Button();

        virtual void            SetCancel(const bool&);
        virtual bool            GetCancel() const;

        virtual void            SetDefaultProperty(const bool&);
        virtual bool            GetDefaultProperty() const;

        virtual bool            GetDefaultedProperty() const;

    protected:
        virtual AutomationPeer  OnCreateAutomationPeer() const; // System.Windows.Automation.Peers.AutomationPeer

    private:
        DependencyProperty      m_IsCancelProperty;
        DependencyProperty      m_IsDefaultProperty;
        DependencyProperty      m_IsDefaultedProperty;
    };
} // namespace Controls