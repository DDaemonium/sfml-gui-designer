#include "Calendar.hpp"

namespace Controls
{
	Calendar::Calendar(): m_IsTodayHighlighted(true)
	{
	}

	Style Calendar::GetCalendarButtonStyle()const
	{
		return m_CalendarButtonStyle;
	}

	Style Calendar::GetCalendarDayButtonStyle()const
	{
		return m_CalendarDayButtonStyle;
	}

	Style Calendar::GetCalendarItemStyle()const
	{
		return m_CalendarItemStyle;
	}

	DateTime Calendar::GetDisplayDate()const
	{
		return m_DisplayDate;
	}

	CalendarSelectionMode Calendar::GetSelectionMode()const
	{
		return m_SelectionMode;
	}

	CalendarMode Calendar::GetDisplayMode()const
	{
		return m_DisplayMode;
	}

	bool Calendar::GetIsTodayHighlighted()const
	{
		return m_IsTodayHighlighted;
	}


	void Calendar::SetCalendarButtonStyle(const Style& calendarButtonStyle)
	{
		m_CalendarButtonStyle = calendarButtonStyle;
	}

	void Calendar::SetCalendarDayButtonStyle(const Style& calendarDayButtonStyle)
	{
		m_CalendarDayButtonStyle = calendarDayButtonStyle;
	}

	void Calendar::SetCalendarItemStyle(const Style& calendarItemStyle)
	{
		m_CalendarItemStyle = calendarItemStyle;
	}

	void Calendar::SetDisplayDate(const DateTime& displayDate)
	{
		m_DisplayDate = displayDate;
	}

	void Calendar::SetDisplayDate(const DateTime& displayDate)
	{
		m_DisplayDate = displayDate;
	}

	void Calendar::SetDisplayMode(const CalendarMode& displayMode)
	{
		m_DisplayMode = displayMode;
	}

	void Calendar::SetIsTodayHighlighted(bool isTodayHighlighted)
	{
		m_IsTodayHighlighted = isTodayHighlighted;
	}

	Automation Calendar::OnCreateAutomationPeer()
	{
	}
	void Calendar::OnDisplayDateChanged()
	{
	}
	void Calendar::OnDisplayModeChanged()
	{
	}
	void Calendar::OnKeyDown()
	{
	}
	void Calendar::OnKeyUp()
	{
	}
	void Calendar::OnSelectedDatesChanged()
	{
	}
	void Calendar::OnSelectionModeChanged()
	{
	}

	void Calendar::OnApplyTemplate()
	{
	}
	std::wstring Calendar::ToString()
	{
	}
}
