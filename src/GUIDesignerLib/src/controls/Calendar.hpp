#pragma once
#include "Control.hpp"


namespace Controls
{
	class Style {};
	class DateTime {};
	class Automation {};


	class Calendar : public Control
	{
	public:
		Calendar();
		virtual ~Calendar();

		virtual void			OnApplyTemplate();
		virtual std::wstring	ToString();

		//Get
		virtual Style					GetCalendarButtonStyle() const;
		virtual Style					GetCalendarDayButtonStyle() const;
		virtual Style					GetCalendarItemStyle() const;
		virtual DateTime				GetDisplayDate() const;
		virtual CalendarSelectionMode	GetSelectionMode() const;
		virtual CalendarMode 			GetDisplayMode() const;
		virtual bool					GetIsTodayHighlighted() const;


		//Set
		virtual void SetCalendarButtonStyle(const Style& calendarButtonStyle);
		virtual void SetCalendarDayButtonStyle(const Style& calendarDayButtonStyle);
		virtual void SetCalendarItemStyle(const Style& calendarItemStyle);
		virtual void SetDisplayDate(const DateTime& displayDate);
		virtual void SetDisplayDate(const DateTime& displayDate);
		virtual void SetDisplayMode(const CalendarMode& displayMode);
		virtual void SetIsTodayHighlighted(bool isTodayHighlighted);

	private:
		Style			m_CalendarButtonStyle;
		Style			m_CalendarDayButtonStyle;
		Style			m_CalendarItemStyle;
		DateTime		m_DisplayDate;
		CalendarSelectionMode m_SelectionMode;				//System.Windows.Controls.CalendarSelectionMode
		CalendarMode	m_displayMode;						//System.Windows.Controls.CalendarMode
		bool			m_IsTodayHighlighted;	

		Event DisplayDate;
		Event DisplayMode.;
		Event SelectedDates;
		Event SelectionMode;

	protected:
		virtual Automation	OnCreateAutomationPeer();		//System.Windows.Automation.Peers.AutomationPeer 
		virtual void		OnDisplayDateChanged();			//System.Windows.Controls.CalendarDateChangedEventArgs e
		virtual void		OnDisplayModeChanged();			//System.Windows.Controls.CalendarModeChangedEventArgs e
		virtual void		OnKeyDown();					//System.Windows.Input.KeyEventArgs e
		virtual void		OnKeyUp();						//System.Windows.Input.KeyEventArgs e
		virtual void		OnSelectedDatesChanged();		//System.Windows.Controls.SelectionChangedEventArgs e
		virtual void		OnSelectionModeChanged();		//EventArgs e
	}
}