#include "ComboBox.hpp"

namespace Controls
{
    ComboBox::ComboBox(): m_IsDropDownOpen (false), m_IsDropDownOpen (false), m_IsEditable (false),
        m_isReadOnly (false), m_ShouldPreserveUserEnteredPrefix (false), m_StaysOpenOnEdit (false)
    {
        
    }
    
    ComboBox::~ComboBox()
    {
    }
    
    DependencyObject* ComboBox::GetContainerForItemOverride () const
    {
        return DependencyObject();
    }
    
    bool ComboBox::IsItemItsOwnContainerOverride (object item)
    {   
        
    }
    
    void ComboBox::OnApplyTemplate ()
    {
        
    }
    
    Automation ComboBox::OnCreateAutomationPeer ()
    {
    
    }
    
    void ComboBox::OnDropDownClosed ()
    {
        
    }
    
    void ComboBox::OnDropDownOpened ()
    {
    
    }   
    
    void ComboBox::OnIsKeyboardFocusWithinChanged ()
    {   
        
    }
    
    void ComboBox::OnKeyDown ()
    {
    
    }   

    void ComboBox::OnMouseLeftButtonUp	()
    {
    
    }
    
    void ComboBox::OnPreviewKeyDown  ()
    {
    
    }
    
    void ComboBox::OnSelectionChanged ()
    {
    
    }
    
    void ComboBox::PrepareContainerForItemOverride (DependencyObject element, object item)
    {
        
    }
    
    
    bool ComboBox::GetIsDropDownOpen() const
    {
        return m_IsDropDownOpen;
    }
    
    void ComboBox::SetIsDropDownOpen(bool isDropDownOpen)
    {
        m_IsDropDownOpen = isDropDownOpen;
    }   
    
    bool ComboBox::GetIsEditable() const
    {
        return m_IsEditable;
    }
    
    void ComboBox::SetIsEditable(bool isEditable)
    {
        m_IsEditable = isEditable;
    }
    
    bool ComboBox::GetIsReadOnly() const
    {
        return m_IsReadOnly;
    }
    
    void ComboBox::SetIsReadOnly(bool isReadOnly)
    {
        m_IsReadOnly = isReadOnly;
    }
    
    double ComboBox::GetMaxDropDownHeight() const
    {
        return m_MaxDropDownHeight;
    }
    
    void ComboBox::SetMaxDropDownHeight(double  maxDropDownHeight)
    {
        m_MaxDropDownHeight = maxDropDownHeight;
    }
    
    ComboBox* ComboBox::GetSelectionBoxItem()
    {
        return this;
    }   
    
    std::string ComboBox::GetSelectionBoxItemStringFormat() const
    {
        return m_SelectionBoxItemStringFormat;
    }
    
    DataTemplate ComboBox::GetSelectionBoxItemTemplate() const
    {
        return m_SelectionBoxItemTemplate;
    }   
    
    bool ComboBox::GetShouldPreserveUserEnteredPrefix() const
    {
        return m_ShouldPreserveUserEnteredPrefix;
    }   
    
    void ComboBox::SetShouldPreserveUserEnteredPrefix(bool shouldPreserveUserEnteredPrefix)
    {
        m_ShouldPreserveUserEnteredPrefix = shouldPreserveUserEnteredPrefix;
    }
    
    bool ComboBox::GetStaysOpenOnEdit() const
    {
        return m_StaysOpenOnEdit;
    }
    
    void ComboBox::SetStaysOpenOnEdit(bool staysOpenOnEdit)
    {
        m_StaysOpenOnEdit = staysOpenOnEdit;
    }   
    
    std::wstring ComboBox::GetText() const
    {
        return m_Text;
    }
    
    void ComboBox::SetText(std::wstring text)
    {
        m_Text = text;
    }
}