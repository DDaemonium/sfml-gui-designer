#include "Control.hpp"

namespace Controls
{
    class DataTemplate{};           //System.Windows.DataTemplate	
    class DependencyObject{};       //System.Windows.DependencyObject
    class Automation{};             //System.Windows.Automation.Peers	

    class ComboBox : public Control
    {
    public:
        ComboBox();
        virtual ~ComboBox();
		
        //Get
        virtual bool            GetIsDropDownOpen() const;
        virtual bool            GetIsEditable() const;
        virtual bool            GetIsReadOnly() const;
        virtual double          GetMaxDropDownHeight() const;
        virtual ComboBox*       GetSelectionBoxItem();
        virtual std::wstring    GetSelectionBoxItemStringFormat() const;
        virtual DataTemplate    GetSelectionBoxItemTemplate() const;			
        virtual bool            GetShouldPreserveUserEnteredPrefix() const;
        virtual bool            GetStaysOpenOnEdit() const;
        virtual std::wstring    GetText() const;

        //Set
        virtual void            SetIsDropDownOpen(bool isDropDownOpen);
        virtual void            SetIsEditable(bool isEditable);
        virtual void            SetIsReadOnly(bool isReadOnly);
        virtual void            SetMaxDropDownHeight(double  maxDropDownHeight);
        virtual void            SetShouldPreserveUserEnteredPrefix(bool shouldPreserveUserEnteredPrefix);
        virtual void            SetStaysOpenOnEdit(bool staysOpenOnEdit);
        virtual void            SetText(std::wstring text);
		
    protected:
        virtual DependencyObject*   GetContainerForItemOverride () const;
        virtual bool                IsItemItsOwnContainerOverride (object item);
        virtual void                OnApplyTemplate ();
        virtual Automation          OnCreateAutomationPeer ();
        virtual void                OnDropDownClosed ();					
        virtual void                OnDropDownOpened ();
        virtual void                OnIsKeyboardFocusWithinChanged ();
        virtual void                OnKeyDown ();
        virtual void                OnMouseLeftButtonUp	();
        virtual void                OnPreviewKeyDown  ();
        virtual void                OnSelectionChanged ();
        virtual void                PrepareContainerForItemOverride (DependencyObject element, object item);

    private:
        bool            m_IsDropDownOpen;
        bool            m_IsEditable;
        bool            m_IsReadOnly;
        double          m_MaxDropDownHeight;
        std::wstring    m_SelectionBoxItemStringFormat;
        DataTemplate    m_SelectionBoxItemTemplate;
        bool            m_ShouldPreserveUserEnteredPrefix;
        bool            m_StaysOpenOnEdit;
        std::wstring    m_Text;

        Event           DropDownClosed;
        Event           DropDownOpened;
    };
}