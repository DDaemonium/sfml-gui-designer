#pragma once
#include "UIElement.hpp"


namespace GUI
{
   /* // Temporary types, usage as alternative of .Net Framework classes for temporary realization the Control class.    
    class Brush {};
    class Thickness {};
    class FontFamily {};
    class FontStyle {};
    class FontStretch {};
    class FontWeight {};
    class HandlesScrolling {};
    class Template {};
    class Padding {};
    class HorizontalAlignment {};
    class VerticalAlignment {};*/

   
    class Control : public UIElement
    {
    public:
        Control();
        virtual ~Control();
/*
        std::wstring                ToString();
        // Events
        void                        MouseDoubleClick();
        void                        PreviewMouseDoubleClick();

        // Getters
        virtual Brush               GetBackground() const;
        virtual Brush               GetBorderBrush() const;
        virtual Thickness           GetBorderThickness() const;
        virtual FontFamily          GetFontFamily() const;
        virtual double              GetFontSize() const;
        virtual FontStretch         GetFontStretch() const;
        virtual FontStyle           GetFontStyle() const;
        virtual FontWeight          GetFontWeight() const;
        virtual Brush               GetForeground() const;
        virtual bool                GetHandlesScrolling() const;
        virtual HorizontalAlignment GetHorizontalContentAlignment() const;
        virtual VerticalAlignment   GetVerticalContentAlignment() const;
        virtual bool                GetTabStopping() const;
        virtual int                 GetTabIndex() const;
        virtual Template            GetTemplate() const;
        virtual Padding             GetPadding() const;

        // Setters
        virtual void                SetBackground(const Brush& background);
        virtual void                SetBorderBrush(const Brush& borderBrush);
        virtual void                SetBorderThickness(const Thickness& borderThickness);
        virtual void                SetFontFamily(const FontFamily& fontFamily);
        virtual void                SetFontSize(double fontSize);
        virtual void                SetFontStretch(const FontStretch& fontStretch);
        virtual void                SetFontWeight(const FontWeight& fontWeight);
        virtual void                SetForeground(const Brush& foreground);
        virtual void                SetHorizontalContentAlignment(const HorizontalAlignment& horzAlignment);
        virtual void                SetVerticalContentAlignment(const VerticalAlignment& vertAlignment);
        virtual void                SetTabStopping(bool tabStopping);
        virtual void                SetTabIndex(int tabIndex);
        virtual void                SetTemplate(const Template& templ);
        virtual void                SetPadding(const Padding& padding);

    protected:
        virtual void                ArrangeOverride();
        virtual void                MeasureOverride();
        virtual void                OnMouseDoubleClick();
        virtual void                OnPreviewMouseDoubleClick();
        virtual void                OnTemplateChanged();

    private:
        Brush                       m_Background;                   // System.Windows.Media.Brush
        Brush                       m_BorderBrush;                  // System.Windows.Media.Brush
        Thickness                   m_BorderThickness;              // System.Windows.Thickness
        FontFamily                  m_FontFamily;                   // System.Windows.Media.FontFamily
        double                      m_FontSize;
        FontStretch                 m_FontStretch;                  // System.Windows.FontStretch
        FontStyle                   m_FontStyle;                    // System.Windows.FontStyle
        FontWeight                  m_FontWeight;                   // System.Windows.FontWeight
        Brush                       m_Foreground;                   // System.Windows.Media.Brush
        bool                        m_HandlesScrolling;
        HorizontalAlignment         m_HorzAlignment;                // System.Windows.HorizontalAlignment
        bool                        m_TabStopping;
        Padding                     m_Padding;                      // System.Windows.Thickness
        int                         m_TabIndex;
        Template                    m_Template;                     // System.Windows.Controls.ControlTemplate
        VerticalAlignment           m_VertAlignment;                // System.Windows.VerticalAlignment

        Event                       m_MouseDoubleClickEvent;
        Event                       m_PreviewMouseDoubleClickEvent;*/
    };
}