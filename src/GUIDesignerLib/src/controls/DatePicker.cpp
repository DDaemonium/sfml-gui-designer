#include "DatePicker.hpp"

namespace Controls
{
    DatePicker::DatePicker(): m_IsDropDownOpen(false), m_IsTodayHighlighted(true)
    {
        //m_DisplayDate  �� ����� Today
        //m_SelectedDateFormat �� ����� long
        // m_SelectedDate �� ����� null
    }
    DatePicker::~DatePicker()
    {
    }

    void DatePicker::OnApplyTemplate()
    {
    }

    std::wstring DatePicker::ToString ()
    {
    }

    Style DatePicker::GetCalendarStyle() const
    {
        return m_CalendarStyle;
    }
	/*
    Nullable<DateTime> DatePicker::GetDisplayDateEnd() const
    {
        return m_DisplayDateEnd;
    }

    DateTime DatePicker::GetDisplayDate() const
    {
        return m_DisplayDate;
    }

    Nullable<DateTime> DatePicker::GetDisplayDateStart() const
    {
        return m_DisplayDateStart;
    }
	*/
    DayOfWeek DatePicker::GetFirstDayOfWeek() const
    {
        return m_FirstDayOfWeek;
    }

    bool DatePicker::GetIsDropDownOpen() const
    {
        return m_IsDropDownOpen;
    }

    bool DatePicker::GetIsTodayHighlighted() const
    {
        return m_SelectedDateFormat;
    }

    Controls.DatePickerFormat DatePicker::GetSelectedDateFormat() const
    {
        return m_SelectedDateFormat;
    }

    Controls.DatePickerFormat DatePicker::GetSelectedDateFormat() const
    {
        return m_SelectedDateFormat;
    }
	/*
    Nullable<DateTime> DatePicker::GetSelectedDate() const
    {
        return m_SelectedDate;
    }
	*/
    std::wstring DatePicker::GetText() const
    {
        return m_Text;
    }

    void DatePicker::SetCalendarStyle(const Style& calendarStyle)
    {
        m_CalendarStyle = calendarStyle;
    }
	/*
    void DatePicker::SetDisplayDateEnd(const Nullable<DateTime>& displayDateEnd)
    {
        m_DisplayDateEnd = displayDateEnd;
    }

    void DatePicker::SetDisplayDate(const DateTime& displayDate)
    {
        m_DisplayDate = displayDate
    }

    void DatePicker::SetDisplayDateStart(const Nullable<DateTime>& displayDateStart)
    {
        m_DisplayDateStart = displayDateStart;
    }
	*/
    void DatePicker::SetFirstDayOfWeek(const DayOfWeek& firstDayOfWeek)
    {
        m_FirstDayOfWeek = FirstDayOfWeek
    }

    void DatePicker::SetIsDropDownOpen(bool isDropDownOpen)
    {
        m_IsDropDownOpen = isDropDownOpen;
    }

    void DatePicker::SetIsTodayHighlighted(bool isTodayHighlighted )
    {
        m_IsTodayHighlighted = isTodayHighlighted;
    }

    void DatePicker::SetSelectedDateFormat(const Controls.DatePickerFormat selectedDateFormat)
    {
        m_SelectedDateFormat = selectedDateFormat;
    }
	/*
    void DatePicker::SetSelectedDate(const Nullable<DateTime>& selectedDate)
    {
        m_SelectedDate = selectedDate;
    }
	*/
    void DatePicker::SetText(std::wstring text)
    {
        m_Text = text;
    }

    void DatePicker::OnCalendarClosed()
    {
    }

    void DatePicker::OnCalendarOpened()
    {
    }

    Automation DatePicker::OnCreateAutomationPeer()
    {
    }

    void DatePicker::OnDateValidationError()
    {
    }
    void DatePicker::OnSelectedDateChanged()
    {
    }
}
