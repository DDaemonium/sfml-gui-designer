#pragma once
#include "Control.hpp"


namespace Controls
{  
    class Style {};
    //class DateTime {};
    class DayOfWeek {};
    class Automation {};
    
    
    class DatePicker : public Control
    {
    public:
        ComboBox();
        virtual ~ComboBox();

        virtual void                OnApplyTemplate();
        virtual std::wstring        ToString ();

        //Get
        virtual Style               GetCalendarStyle() const;
        //virtual Nullable<DateTime>  GetDisplayDateEnd() const;
        virtual DateTime            GetDisplayDate() const;
        //virtual Nullable<DateTime>  GetDisplayDateStart() const;
        virtual DayOfWeek           GetFirstDayOfWeek() const;
        virtual bool                GetIsDropDownOpen() const;
        virtual bool                GetIsTodayHighlighted() const;
        virtual Controls.DatePickerFormat GetSelectedDateFormat() const;
        virtual Controls.DatePickerFormat GetSelectedDateFormat() const;
        //virtual Nullable<DateTime>  GetSelectedDate() const;
        virtual std::wstring        GetText() const;

        //Set
        virtual void                SetCalendarStyle(const Style& calendarStyle);
        //virtual void                SetDisplayDateEnd(const Nullable<DateTime>& displayDateEnd);
        virtual void                SetDisplayDate(const DateTime& displayDate);                
        //virtual void                SetDisplayDateStart(const Nullable<DateTime>& displayDateStart);
        virtual void                SetFirstDayOfWeek(const DayOfWeek& firstDayOfWeek);
        virtual void                SetIsDropDownOpen(bool isDropDownOpen);
        virtual void                SetIsTodayHighlighted(bool isTodayHighlighted );
        virtual void                SetSelectedDateFormat(const Controls.DatePickerFormat selectedDateFormat);  
        //virtual void                SetSelectedDate(const Nullable<DateTime>& selectedDate);
        virtual void                SetText(std::wstring text);
    

    protected:
        virtual void        OnCalendarClosed();         //event
        virtual void        OnCalendarOpened();         //event
        virtual Automation  OnCreateAutomationPeer();   //System.Windows.Automation.Peers.AutomationPeer
        virtual void        OnDateValidationError();    //event
        virtual void        OnSelectedDateChanged();    //event

    private:
        Style               m_CalendarStyle; 
        //Nullable<DateTime>  m_DisplayDateEnd;   
        DateTime            m_DisplayDate
        //Nullable<DateTime>  m_DisplayDateStart;
        DayOfWeek           m_FirstDayOfWeek;
        bool                m_IsDropDownOpen;
        bool                m_IsTodayHighlighted;
        Controls.DatePickerFormat m_SelectedDateFormat; //System.Windows.Controls.DatePickerFormat
        //Nullable<DateTime>  m_SelectedDate;
        std::wstring        m_Text

        Event CalendarClosed;       //System.Windows.RoutedEventHandler
        Event CalendarOpened;       //System.Windows.RoutedEventHandler 
        Event DateValidationError;  //System.Windows.Controls.DatePickerDateValidationErrorEventArgs
        Event SelectedDateChanged;  //System.Windows.Controls.SelectionChangedEventArgs
    };
}
