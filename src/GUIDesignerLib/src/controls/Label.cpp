#include "Label.hpp"


namespace Controls
{
    Label::Label()
    {
    }

    Label::~Label()
    {
    }

    UIElement* Label::GetTarget() const
    {
        return m_Target;
    }

    void Label::SetTarget(UIElement* target)
    {
        m_Target = target;
    }

    AutomationPeer Label::OnCreateAutomationPeer()
    {
        return AutomationPeer();
    }
}