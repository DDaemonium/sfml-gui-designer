#pragma once
#include "Control.hpp"


namespace Controls
{
    class AutomationPeer {};
    
    // Parents of this class: Control, ContentControl, FrameworkElement, DispatcherObject, Visual
    class Label : public Control
    {
    public:
        Label();
        virtual ~Label();

        virtual UIElement*      GetTarget() const;
        virtual void            SetTarget(UIElement* target);

    protected:
        virtual AutomationPeer  OnCreateAutomationPeer();   // System.Windows.Automation.Peers.AutomationPeer

    private:
        UIElement*              m_Target;                   // TODO: Replace with smart-pointer
    };
}