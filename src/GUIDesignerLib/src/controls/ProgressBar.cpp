#include "ProgressBar.hpp"


namespace Controls
{
    ProgressBar::ProgressBar()
    {
    }

    ProgressBar::~ProgressBar()
    {
    }

    Orientation ProgressBar::GetOrientation() const
    {
        return m_Orientation;
    }

    bool ProgressBar::GetIndeterminate() const
    {
        return m_IsIndeterminate;
    }

    void ProgressBar::SetOrientation(const Orientation& orientation)
    {
        m_Orientation = orientation;
    }

    void ProgressBar::SetIndeterminate(bool indeterminate)
    {
        m_IsIndeterminate = indeterminate;
    }

    void ProgressBar::OnApplyTemplate()
    {
    }

    AutomationPeer ProgressBar::OnCreateAutomationPeer()
    {
        return AutomationPeer();
    }

    void ProgressBar::OnMaximumChanged(double oldMaximum, double newMaximum)
    {
    }

    void ProgressBar::OnMinimumChanged(double oldMinimum, double newMinimum)
    {
    }

    void ProgressBar::OnValueChanged(double oldValue, double newValue)
    {
    }
}