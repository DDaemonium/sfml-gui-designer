#pragma once
#include "Control.hpp"


namespace Controls
{
    class Orientation {};
    class AutomationPeer {};
    

    class ProgressBar : public Control
    {
    public:
        ProgressBar();
        virtual ~ProgressBar();

        virtual Orientation     GetOrientation() const;
        virtual bool            GetIndeterminate() const;

        virtual void            SetOrientation(const Orientation& orientation);
        virtual void            SetIndeterminate(bool indeterminate);

        virtual void            OnApplyTemplate();        

    protected:
        virtual AutomationPeer  OnCreateAutomationPeer();
        virtual void            OnMaximumChanged(double oldMaximum, double newMaximum);
        virtual void            OnMinimumChanged(double oldMinimum, double newMinimum);
        virtual void            OnValueChanged(double oldValue, double newValue);

    private:
        bool                    m_IsIndeterminate;
        Orientation             m_Orientation;          // System.Windows.Controls.Orientation
    };
}