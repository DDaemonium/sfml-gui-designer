#include "TextBox.hpp"


namespace Controls
{
    TextBox::TextBox()
    {
    }

    TextBox::~TextBox()
    {
    }

    void TextBox::Clear()
    {
    }

    Size TextBox::MeasureOverride()
    {
        return Size();
    }

    void TextBox::ScrollToLine()
    {
    }

    void TextBox::Select()
    {
    }

    bool TextBox::ShouldSerializeText()
    {
        return false;
    }

    int TextBox::GetCharacterIndexFromLineIndex()
    {
        return 0;
    }

    int TextBox::GetCharacterIndexFromPoint()
    {
        return 0;
    }

    int TextBox::GetFirstVisibleLineIndex()
    {
        return 0;
    }

    int TextBox::GetlastVisibleLineIndex()
    {
        return 0;
    }

    int TextBox::GetLineIndexFromCharIndex()
    {
        return 0;
    }

    int TextBox::GetLineLength()
    {
        return 0;
    }

    std::wstring TextBox::GetLineText()
    {
        return std::wstring();
    }

    int TextBox::GetNextSpellingErrorCharacterIndex()
    {
        return 0;
    }

    Rect TextBox::GetRectFromCharacterIndex()
    {
        return Rect();
    }

    SpellingError TextBox::GetSpellingError()
    {
        return SpellingError();
    }

    int TextBox::GetSpellingErrorLength()
    {
        return 0;
    }

    int TextBox::GetSpellingErrorStart()
    {
        return 0;
    }

    int TextBox::GetCaretIndex() const
    {
        return m_CaretIndex;
    }

    CharacterCasing TextBox::GetCharacterCasing() const
    {
        return m_CharacterCasing;
    }

    int TextBox::GetLineCount() const
    {
        return m_LineCount;
    }

    IEnumerator TextBox::GetLogicalChildren() const
    {
        return m_LogicalChildren;
    }

    int TextBox::GetMaxLength() const
    {
        return m_MaxLength;
    }

    int TextBox::GetMaxLines() const
    {
        return m_MaxLines;
    }

    int TextBox::GetMinLines() const
    {
        return m_MinLines;
    }

    std::wstring TextBox::GetSelectedText() const
    {
        return m_SelectedText;
    }

    int TextBox::GetSelectionLength() const
    {
        return m_SelectionLength;
    }

    int TextBox::GetSelectionStart() const
    {
        return m_SelectionStart;
    }

    std::wstring TextBox::GetText() const
    {
        return m_Text;
    }

    TextAlignment TextBox::GetTextAlignment() const
    {
        return m_TextAlignment;
    }

    TextDecorationCollection TextBox::GetTextDecorations() const
    {
        return m_TextDecorations;
    }

    TextWrapping TextBox::GetTextWrapping() const
    {
        return m_TextWrapping;
    }

    Typography TextBox::GetTypography() const
    {
        return m_Typography;
    }

    void TextBox::SetCaretIndex(int caretIndex)
    {
        m_CaretIndex = caretIndex;
    }

    void TextBox::SetCharacterCasing(const CharacterCasing& characterCasing)
    {
        m_CharacterCasing = characterCasing;
    }

    void TextBox::SetMaxLength(int maxLength)
    {
        m_MaxLength = maxLength;
    }

    void TextBox::SetMaxLines(int maxLines)
    {
        m_MaxLines = maxLines;
    }

    void TextBox::SetMinLines(int minLines)
    {
        m_MinLines = minLines;
    }

    void TextBox::SetSelectedText(const std::wstring& selectedText)
    {
        m_SelectedText = selectedText;
    }

    void TextBox::SetSelectionLength(int selectionLength)
    {
        m_SelectionLength = selectionLength;
    }

    void TextBox::SetSelectionStart(int selectionStart)
    {
        m_SelectionStart = selectionStart;
    }

    void TextBox::SetText(const std::wstring& text)
    {
        m_Text = text;
    }

    void TextBox::SetTextAlignment(const TextAlignment& textAlignment)
    {
        m_TextAlignment = textAlignment;
    }

    void TextBox::SetTextDecorations(const TextDecorationCollection& textDecorations)
    {
        m_TextDecorations = textDecorations;
    }

    void TextBox::SetTextWrapping(const TextWrapping& textWrapping)
    {
        m_TextWrapping = textWrapping;
    }

    AutomationPeer TextBox::OnCreateAutomationPeer()
    {
        return AutomationPeer();
    }

    void TextBox::OnPropertyChange()
    {
    }
}