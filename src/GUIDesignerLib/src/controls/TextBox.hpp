#pragma once
#include "Control.hpp"


namespace Controls
{
    class CharacterCasing{};
    class IEnumerator{};
    class TextAlignment{};
    class TextDecorationCollection{};
    class TextWrapping{};
    class Typography{};
    class SpellingError{};
    class AutomationPeer{};
    class Rect{};

    
    class TextBox : public Control
    {
    public:
        TextBox();
        virtual ~TextBox();

        virtual void                Clear();
        virtual Size                MeasureOverride();
        virtual void                ScrollToLine();
        virtual void                Select();
        virtual bool                ShouldSerializeText();

        virtual int                 GetCharacterIndexFromLineIndex();
        virtual int                 GetCharacterIndexFromPoint();
        virtual int                 GetFirstVisibleLineIndex();
        virtual int                 GetlastVisibleLineIndex();
        virtual int                 GetLineIndexFromCharIndex();
        virtual int                 GetLineLength();
        virtual std::wstring        GetLineText();
        virtual int                 GetNextSpellingErrorCharacterIndex();
        virtual Rect                GetRectFromCharacterIndex();
        virtual SpellingError       GetSpellingError();
        virtual int                 GetSpellingErrorLength();
        virtual int                 GetSpellingErrorStart();

        // Getters
        virtual int                 GetCaretIndex() const;
        virtual CharacterCasing     GetCharacterCasing() const;
        virtual int                 GetLineCount() const;
        virtual IEnumerator         GetLogicalChildren() const;
        virtual int                 GetMaxLength() const;
        virtual int                 GetMaxLines() const;
        virtual int                 GetMinLines() const;
        virtual std::wstring        GetSelectedText() const;
        virtual int                 GetSelectionLength() const;
        virtual int                 GetSelectionStart() const;
        virtual std::wstring        GetText() const;
        virtual TextAlignment       GetTextAlignment() const;
        virtual TextDecorationCollection     GetTextDecorations() const;
        virtual TextWrapping        GetTextWrapping() const;
        virtual Typography          GetTypography() const;       

        // Setters
        virtual void                SetCaretIndex(int caretIndex);
        virtual void                SetCharacterCasing(const CharacterCasing& characterCasing);
        virtual void                SetMaxLength(int maxLength);
        virtual void                SetMaxLines(int maxLines);
        virtual void                SetMinLines(int minLines);
        virtual void                SetSelectedText(const std::wstring& selectedText);
        virtual void                SetSelectionLength(int selectionLength);
        virtual void                SetSelectionStart(int selectionStart);
        virtual void                SetText(const std::wstring& text);
        virtual void                SetTextAlignment(const TextAlignment& textAlignment);
        virtual void                SetTextDecorations(const TextDecorationCollection& textDecorations);
        virtual void                SetTextWrapping(const TextWrapping& textWrapping);

    protected:
        virtual AutomationPeer      OnCreateAutomationPeer();
        virtual void                OnPropertyChange();

    private:
        int                         m_CaretIndex;
        CharacterCasing             m_CharacterCasing;      // System.Windows.Controls.CharacterCasing
        int                         m_LineCount;
        IEnumerator                 m_LogicalChildren;      // System.Collections.IEnumerator
        int                         m_MaxLength;
        int                         m_MaxLines;
        int                         m_MinLines;
        std::wstring                m_SelectedText;
        int                         m_SelectionLength;
        int                         m_SelectionStart;
        std::wstring                m_Text;
        TextAlignment               m_TextAlignment;        // System.Windows.TextAlignment
        TextDecorationCollection    m_TextDecorations;      // System.Windows.TextDecorationCollection
        TextWrapping                m_TextWrapping;         // System.Windows.TextWrapping
        Typography                  m_Typography;           // System.Windows.Documents.Typography
    }; 
}