#include "ToolBar.hpp"


namespace Controls
{
    ToolBar::ToolBar()
    {
    }

    ToolBar::~ToolBar()
    {
    }

    bool ToolBar::GetIsOverflowItem()
    {
        return false;
    }

    OverflowMode ToolBar::GetOverflowMode()
    {
        return OverflowMode();
    }

    void ToolBar::SetOverflowMode()
    {
    }

    int ToolBar::GetBand() const
    {
        return m_Band;
    }

    int ToolBar::GetBandIndex() const
    {
        return m_BandIndex;
    }

    ResourceKey ToolBar::GetButtonStyleKey() const
    {
        return m_ButtonStyleKey;
    }

    ResourceKey ToolBar::GetCheckBoxStyleKey() const
    {
        return m_CheckBoxStyleKey;
    }

    ResourceKey ToolBar::GetComboBoxStyleKey() const
    {
        return m_ComboBoxStyleKey;
    }

    ResourceKey ToolBar::GetMenuStyleKey() const
    {
        return m_MenuStyleKey;
    }

    ResourceKey ToolBar::GetRadioButtonStyleKey() const
    {
        return m_RadioButtonStyleKey;
    }

    ResourceKey ToolBar::GetSeparatorStyleKey() const
    {
        return m_SeparatorStyleKey;
    }

    ResourceKey ToolBar::GetTextBoxStyleKey() const
    {
        return m_TextBoxStyleKey;
    }

    ResourceKey ToolBar::GetToggleButtonStyleKey() const
    {
        return m_ToggleButtonStyleKey;
    }

    Orientation ToolBar::GetOrientation() const
    {
        return m_Orientation;
    }

    bool ToolBar::GetIsOverflowItems() const
    {
        return m_HasOverflowItems;
    }

    bool ToolBar::GetIsOverflowOpen() const
    {
        return m_IsOverflowOpen;
    }

    void ToolBar::SetBand(int band)
    {
        m_Band = band;
    }

    void ToolBar::SetBandIndex(int bandIndex)
    {
        m_BandIndex = bandIndex;
    }

    void ToolBar::SetIsOverflowOpen(bool overflowOpen)
    {
        m_IsOverflowOpen = overflowOpen;
    }

    Size ToolBar::MeasureOverride()
    {
        return Size();
    }

    AutomationPeer ToolBar::OnCreateAutomationPeer()
    {
        return AutomationPeer();
    }

    void ToolBar::OnItemsChanged()
    {
    }

    void ToolBar::OnKeyDown()
    {
    }

    void ToolBar::OnLostMouseCapture()
    {
    }

    void ToolBar::PrepareContainerForItemOverride()
    {
    }
}