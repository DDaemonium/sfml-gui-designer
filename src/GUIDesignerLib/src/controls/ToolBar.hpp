#pragma once
#include "Control.hpp"


namespace Controls
{
    class AutomationPeer{};
    class Orientation{};
    class OverflowMode{};
    class ResourceKey{};
    
    
    class ToolBar : public Control
    {
    public:
        ToolBar();
        virtual ~ToolBar();

        virtual bool            GetIsOverflowItem();
        virtual OverflowMode    GetOverflowMode();
        virtual void            SetOverflowMode();

        // Getters
        virtual int             GetBand() const;
        virtual int             GetBandIndex() const;
        virtual ResourceKey     GetButtonStyleKey() const;
        virtual ResourceKey     GetCheckBoxStyleKey() const;
        virtual ResourceKey     GetComboBoxStyleKey() const;
        virtual ResourceKey     GetMenuStyleKey() const;
        virtual ResourceKey     GetRadioButtonStyleKey() const;
        virtual ResourceKey     GetSeparatorStyleKey() const;
        virtual ResourceKey     GetTextBoxStyleKey() const;
        virtual ResourceKey     GetToggleButtonStyleKey() const;
        virtual Orientation     GetOrientation() const;
        virtual bool            GetIsOverflowItems() const;
        virtual bool            GetIsOverflowOpen() const;

        // Setters
        virtual void            SetBand(int band);
        virtual void            SetBandIndex(int bandIndex);
        virtual void            SetIsOverflowOpen(bool overflowOpen);

    protected:
        virtual Size            MeasureOverride();
        virtual AutomationPeer  OnCreateAutomationPeer();
        virtual void            OnItemsChanged();
        virtual void            OnKeyDown();
        virtual void            OnLostMouseCapture();
        virtual void            PrepareContainerForItemOverride();

    private:
        int                     m_Band;
        int                     m_BandIndex;
        ResourceKey             m_ButtonStyleKey;       // System.Windows.ResourceKey
        ResourceKey             m_CheckBoxStyleKey;
        ResourceKey             m_ComboBoxStyleKey;
        bool                    m_HasOverflowItems;
        bool                    m_IsOverflowOpen;
        ResourceKey             m_MenuStyleKey;
        Orientation             m_Orientation;
        ResourceKey             m_RadioButtonStyleKey;
        ResourceKey             m_SeparatorStyleKey;
        ResourceKey             m_TextBoxStyleKey;
        ResourceKey             m_ToggleButtonStyleKey;
    };
}