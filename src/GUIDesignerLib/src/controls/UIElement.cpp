#include "UIElement.hpp"


namespace GUI
{
    UIElement::UIElement()
    {
    }

    UIElement::~UIElement()
    {
    }
/*
    void UIElement::DragEnter()
    {
    }

    void UIElement::DragLeave()
    {
    }

    void UIElement::DragOver()
    {
    }

    void UIElement::Drop()
    {
    }

    void UIElement::FocusableChanged()
    {
    }

    void UIElement::GiveFeedback()
    {
    }

    void UIElement::GotFocus()
    {
    }

    void UIElement::GotKeyboardFocus()
    {
    }

    void UIElement::GotMouseCapture()
    {
    }

    void UIElement::IsEnableChanged()
    {
    }

    void UIElement::IsHitTestVisibleChanged()
    {
    }

    void UIElement::IsKeyboardFocusedChanged()
    {
    }

    void UIElement::IsKeyboardFocusWithinChanged()
    {
    }

    void UIElement::IsMouseCapturedChanged()
    {
    }

    void UIElement::IsMouseCaptureWithinChanged()
    {
    }

    void UIElement::IsMouseDirectlyOverChanged()
    {
    }

    void UIElement::IsVisibleChanged()
    {
    }

    void UIElement::KeyDown()
    {
    }

    void UIElement::KeyUp()
    {
    }

    void UIElement::LayoutUpdated()
    {
    }

    void UIElement::LostFocus()
    {
    }

    void UIElement::LostKeyboardFocus()
    {
    }

    void UIElement::LostMouseCapture()
    {
    }

    void UIElement::ManipulationBoundaryFeedback()
    {
    }

    void UIElement::ManipulationCompleted()
    {
    }

    void UIElement::ManipulationDelta()
    {
    }

    void UIElement::ManipulationInertiaStarting()
    {
    }

    void UIElement::ManipulationStarted()
    {
    }

    void UIElement::ManipulationStarting()
    {
    }

    void UIElement::MouseDown()
    {
    }

    void UIElement::MouseEnter()
    {
    }

    void UIElement::MouseLeave()
    {
    }

    void UIElement::MouseLeftButtonDown()
    {
    }

    void UIElement::MouseLeftButtonUp()
    {
    }

    void UIElement::MouseMove()
    {
    }

    void UIElement::MouseRightButtonDown()
    {
    }

    void UIElement::MouseRightButtonUp()
    {
    }

    void UIElement::MouseUp()
    {
    }

    void UIElement::MouseWheel()
    {
    }

    void UIElement::PreviewDragEnter()
    {
    }

    void UIElement::PreviewDragLeave()
    {
    }

    void UIElement::PreviewDragOver()
    {
    }

    void UIElement::PreviewDrop()
    {
    }

    void UIElement::PreviewGiveFeedback()
    {
    }

    void UIElement::PreviewGotKeyboardFocus()
    {
    }

    void UIElement::PreviewKeyDown()
    {
    }

    void UIElement::PreviewKeyUp()
    {
    }

    void UIElement::PreviewLostKeyboardFocus()
    {
    }

    void UIElement::PreviewMouseDown()
    {
    }

    void UIElement::PreviewMouseLeftButtonDown()
    {
    }

    void UIElement::PreviewMouseLeftButtonUp()
    {
    }

    void UIElement::PreviewMouseMove()
    {
    }

    void UIElement::PreviewMouseRightButtonDown()
    {
    }

    void UIElement::PreviewMouseRightButtonUp()
    {
    }

    void UIElement::PreviewMouseUp()
    {
    }

    void UIElement::PreviewMouseWheel()
    {
    }

    void UIElement::PreviewQueryContinueDrag()
    {
    }

    void UIElement::PreviewTextInput()
    {
    }

    void UIElement::QueryContinueDrag()
    {
    }

    void UIElement::QueryCursor()
    {
    }

    void UIElement::TextInput()
    {
    }

    bool UIElement::GetAllowDrop() const
    {
        return false;
    }

    CacheMode UIElement::GetCacheMode() const
    {
        return CacheMode();
    }

    Geometry UIElement::GetClip() const
    {
        return Geometry();
    }

    bool UIElement::GetClipToBounds() const
    {
        return false;
    }

    CommandBindingCollection UIElement::GetCommandBindings() const
    {
        return CommandBindingCollection();
    }

    Size UIElement::GetDesiredSize() const
    {
        return Size();
    }

    Effect UIElement::GetEffect() const
    {
        return Effect();
    }

    bool UIElement::GetFocusable() const
    {
        return false;
    }

    bool UIElement::GetAnimatedProperties() const
    {
        return false;
    }

    bool UIElement::GetEffectiveKeyboardFocus() const
    {
        return false;
    }

    InputBindingCollection UIElement::GetInputBindings() const
    {
        return InputBindingCollection();
    }

    bool UIElement::GetArrangeValid() const
    {
        return false;
    }

    bool UIElement::GetEnabled() const
    {
        return false;
    }

    bool UIElement::GetEnabledCore() const
    {
        return false;
    }

    bool UIElement::GetFocused() const
    {
        return false;
    }

    bool UIElement::GetHitTestVisible() const
    {
        return false;
    }

    bool UIElement::GetInputMethodEnabled() const
    {
        return false;
    }

    bool UIElement::GetKeyboardFocused() const
    {
        return false;
    }

    bool UIElement::GetKeyboardFocusWithin() const
    {
        return false;
    }

    bool UIElement::GetManipulationEnabled() const
    {
        return false;
    }

    bool UIElement::GetMeasureValid() const
    {
        return false;
    }

    bool UIElement::GetMouseCaptured() const
    {
        return false;
    }

    bool UIElement::GetMouseCaptureWithin() const
    {
        return false;
    }

    bool UIElement::GetMouseDirectlyOver() const
    {
        return false;
    }

    bool UIElement::GetMouseOver() const
    {
        return false;
    }

    bool UIElement::GetVisible() const
    {
        return false;
    }

    double UIElement::GetOpacity() const
    {
        return 0.0;
    }

    Brush UIElement::GetOpacityMask() const
    {
        return Brush();
    }

    Size UIElement::GetRenderSize() const
    {
        return Size();
    }

    Transform UIElement::GetRenderTransform() const
    {
        return Transform();
    }

    Point UIElement::GetRenderTransformOrigin() const
    {
        return Point();
    }

    bool UIElement::GetSnapsToDevicePixels() const
    {
        return false;
    }

    std::wstring UIElement::GetUID() const
    {
        return std::wstring();
    }

    Visibility UIElement::GetVisibility() const
    {
        return Visibility();
    }

    void UIElement::SetAllowDrop(bool allowDrop)
    {
        m_AllowDrop = allowDrop;
    }

    void UIElement::SetCacheMode(const CacheMode& cacheMode)
    {
        m_CacheMode = cacheMode;
    }

    void UIElement::SetClip(const Geometry& clip)
    {
        m_Clip = clip;
    }

    void UIElement::SetClipToBounds(bool clipToBounds)
    {
        m_ClipToBounds = clipToBounds;
    }

    void UIElement::SetEffect(const Effect& effect)
    {
        m_Effect = effect;
    }

    void UIElement::SetFocusable(bool focusable)
    {
        m_Focusable = focusable;
    }

    void UIElement::SetInputBindings(const InputBindingCollection& inputBindings)
    {
        m_InputBindings = inputBindings;
    }

    void UIElement::SetEnabled(bool isEnabled)
    {
        m_IsEnabled = isEnabled;
    }

    void UIElement::SetHitTestVisible(bool hitTestVisible)
    {
        m_IsHitTestVisible = hitTestVisible;
    }

    void UIElement::SetManipulationEnabled(bool isManipEnabled)
    {
        m_IsManipulationEnabled = isManipEnabled;
    }

    void UIElement::SetOpacity(double opacity)
    {
        m_Opacity = opacity;
    }

    void UIElement::SetOpacityMask(const Brush& opacityMask)
    {
        m_OpacityMask = opacityMask;
    }

    void UIElement::SetRenderSize(const Size& renderSize)
    {
        m_RenderSize = renderSize;
    }

    void UIElement::SetRenderTransform(const Transform& renderTransform)
    {
        m_RenderTransform = renderTransform;
    }

    void UIElement::SetRenderTransformOrigin(const Point& transformOrigin)
    {
        m_RenderTransformOrigin = transformOrigin;
    }

    void UIElement::SetSnapsToDevicePixels(bool snapsToPixels)
    {
        m_SnapsToDevicePixels = snapsToPixels;
    }

    void UIElement::SetUID(const std::wstring& uID)
    {
        m_UID = uID;
    }

    void UIElement::SetVisibility(const Visibility& visibility)
    {
        m_Visibility = visibility;
    }

    void UIElement::AddHandler()
    {
    }

    void UIElement::AddToEventRoute()
    {
    }

    void UIElement::ApplyAnimationClock()
    {
    }

    void UIElement::Arrange()
    {
    }

    void UIElement::ArrangeCore()
    {
    }

    void UIElement::BeginAnimation()
    {
    }

    void UIElement::CaptureMouse()
    {
    }

    void UIElement::Focus()
    {
    }

    void UIElement::GetAnimationBaseValue()
    {
    }

    void UIElement::GetLayoutClip()
    {
    }

    void UIElement::GetUIParentCore()
    {
    }

    void UIElement::HitTestCore()
    {
    }

    void UIElement::InputHitTest()
    {
    }

    void UIElement::InvalidateArrange()
    {
    }

    void UIElement::InvalidateMeasure()
    {
    }

    void UIElement::InvalidateVisual()
    {
    }

    void UIElement::Measure()
    {
    }

    void UIElement::MeasureCore()
    {
    }

    void UIElement::MoveFocus()
    {
    }

    void UIElement::PredictFocus()
    {
    }

    void UIElement::RaiseEvent()
    {
    }

    void UIElement::ReleaseMouseCapture()
    {
    }

    void UIElement::RemoveHandler()
    {
    }

    void UIElement::ShouldSerializeCommandBindings()
    {
    }

    void UIElement::ShouldSerializeInputBindings()
    {
    }

    void UIElement::TranslatePoint()
    {
    }

    void UIElement::UpdateLayout()
    {
    }

    void UIElement::OnAccessKey()
    {
    }

    void UIElement::OnChildDesiredSizeChanged()
    {
    }

    void UIElement::OnCreateAutomationPeer()
    {
    }

    void UIElement::OnDragEnter()
    {
    }

    void UIElement::OnDragLeave()
    {
    }

    void UIElement::OnDragOver()
    {
    }

    void UIElement::OnDrop()
    {
    }

    void UIElement::OnGiveFeedback()
    {
    }

    void UIElement::OnGotFocus()
    {
    }

    void UIElement::OnGotKeyboardFocus()
    {
    }

    void UIElement::OnGotMouseCapture()
    {
    }

    void UIElement::OnIsKeyboardFocusedChanged()
    {
    }

    void UIElement::OnIsKeyboardFocusWithinChanged()
    {
    }

    void UIElement::OnIsMouseCapturedChanged()
    {
    }

    void UIElement::OnIsMouseCaptureWithinChanged()
    {
    }

    void UIElement::OnIsMouseDirectlyOverChanged()
    {
    }

    void UIElement::OnKeyDown()
    {
    }

    void UIElement::OnKeyUp()
    {
    }

    void UIElement::OnLostFocus()
    {
    }

    void UIElement::OnLostKeyboardFocus()
    {
    }

    void UIElement::OnLostMouseCapture()
    {
    }

    void UIElement::OnManipulationBoundaryFeedback()
    {
    }

    void UIElement::OnManipulationCompleted()
    {
    }

    void UIElement::OnManipulationDelta()
    {
    }

    void UIElement::OnManipulationInertiaStarting()
    {
    }

    void UIElement::OnManipulationStarted()
    {
    }

    void UIElement::OnManipulationStarting()
    {
    }

    void UIElement::OnMouseDown()
    {
    }

    void UIElement::OnMouseEnter()
    {
    }

    void UIElement::OnMouseLeave()
    {
    }

    void UIElement::OnMouseLeftButtonDown()
    {
    }

    void UIElement::OnMouseLeftButtonUp()
    {
    }

    void UIElement::OnMouseMove()
    {
    }

    void UIElement::OnMouseRightButtonDown()
    {
    }

    void UIElement::OnMouseRightButtonUp()
    {
    }

    void UIElement::OnMouseUp()
    {
    }

    void UIElement::OnMouseWheel()
    {
    }

    void UIElement::OnPreviewDragEnter()
    {
    }

    void UIElement::OnPreviewDragLeave()
    {
    }

    void UIElement::OnPreviewDragOver()
    {
    }

    void UIElement::OnPreviewDrop()
    {
    }

    void UIElement::OnPreviewGiveFeedback()
    {
    }

    void UIElement::OnPreviewGotKeyboardFocus()
    {
    }

    void UIElement::OnPreviewKeyDown()
    {
    }

    void UIElement::OnPreviewKeyUp()
    {
    }

    void UIElement::OnPreviewLostKeyboardFocus()
    {
    }

    void UIElement::OnPreviewMouseDown()
    {
    }

    void UIElement::OnPreviewMouseLeftButtonDown()
    {
    }

    void UIElement::OnPreviewMouseLeftButtonUp()
    {
    }

    void UIElement::OnPreviewMouseMove()
    {
    }

    void UIElement::OnPreviewMouseRightButtonDown()
    {
    }

    void UIElement::OnPreviewMouseRightButtonUp()
    {
    }

    void UIElement::OnPreviewMouseUp()
    {
    }

    void UIElement::OnPreviewMouseWheel()
    {
    }

    void UIElement::OnPreviewQueryContinueDrag()
    {
    }

    void UIElement::OnPreviewTextInput()
    {
    }

    void UIElement::OnQueryContinueDrag()
    {
    }

    void UIElement::OnQueryCursor()
    {
    }

    void UIElement::OnRender()
    {
    }

    void UIElement::OnRenderSizeChanged()
    {
    }

    void UIElement::OnTextInput()
    {
    }

    void UIElement::OnVisualParentChanged()
    {
    }*/

}