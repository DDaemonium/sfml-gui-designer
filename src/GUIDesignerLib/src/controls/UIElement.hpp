#pragma once
#include <string>
#include "../utility/CustomTypes.hpp"

namespace GUI
{
   /* // TemporaryClasses
    class Brush {};
    class CacheMode {};
    class Geometry {};
    class CommandBindingCollection {};
    class Size {};
    class Effect {};
    class InputBindingCollection {};
    class Transform {};
    class Point {};
    class Visibility {};
    // Temporary Event class
    class Event {};*/

    
    class UIElement
    {
    public:
        UIElement();
        virtual ~UIElement();
/*
        // Events:
        virtual void            DragEnter();
        virtual void            DragLeave();
        virtual void            DragOver();
        virtual void            Drop();
        virtual void            FocusableChanged();
        virtual void            GiveFeedback();
        virtual void            GotFocus();
        virtual void            GotKeyboardFocus();
        virtual void            GotMouseCapture();
        virtual void            IsEnableChanged();
        virtual void            IsHitTestVisibleChanged();
        virtual void            IsKeyboardFocusedChanged();
        virtual void            IsKeyboardFocusWithinChanged();
        virtual void            IsMouseCapturedChanged();
        virtual void            IsMouseCaptureWithinChanged();
        virtual void            IsMouseDirectlyOverChanged();
        virtual void            IsVisibleChanged();
        virtual void            KeyDown();
        virtual void            KeyUp();
        virtual void            LayoutUpdated();
        virtual void            LostFocus();
        virtual void            LostKeyboardFocus();
        virtual void            LostMouseCapture();
        virtual void            ManipulationBoundaryFeedback();
        virtual void            ManipulationCompleted();
        virtual void            ManipulationDelta();
        virtual void            ManipulationInertiaStarting();
        virtual void            ManipulationStarted();
        virtual void            ManipulationStarting();
        virtual void            MouseDown();
        virtual void            MouseEnter();
        virtual void            MouseLeave();
        virtual void            MouseLeftButtonDown();
        virtual void            MouseLeftButtonUp();
        virtual void            MouseMove();
        virtual void            MouseRightButtonDown();
        virtual void            MouseRightButtonUp();
        virtual void            MouseUp();
        virtual void            MouseWheel();
        virtual void            PreviewDragEnter();
        virtual void            PreviewDragLeave();
        virtual void            PreviewDragOver();
        virtual void            PreviewDrop();
        virtual void            PreviewGiveFeedback();
        virtual void            PreviewGotKeyboardFocus();
        virtual void            PreviewKeyDown();
        virtual void            PreviewKeyUp();
        virtual void            PreviewLostKeyboardFocus();
        virtual void            PreviewMouseDown();
        virtual void            PreviewMouseLeftButtonDown();
        virtual void            PreviewMouseLeftButtonUp();
        virtual void            PreviewMouseMove();
        virtual void            PreviewMouseRightButtonDown();
        virtual void            PreviewMouseRightButtonUp();
        virtual void            PreviewMouseUp();
        virtual void            PreviewMouseWheel();
        virtual void            PreviewQueryContinueDrag();
        virtual void            PreviewTextInput();
        virtual void            QueryContinueDrag();
        virtual void            QueryCursor();
        virtual void            TextInput();

        // Getters
        virtual bool            GetAllowDrop() const;
        virtual CacheMode       GetCacheMode() const;
        virtual Geometry        GetClip() const;
        virtual bool            GetClipToBounds() const;
        virtual CommandBindingCollection    GetCommandBindings() const;
        virtual Size            GetDesiredSize() const;
        virtual Effect          GetEffect() const;
        virtual bool            GetFocusable() const;
        virtual bool            GetAnimatedProperties() const;
        virtual bool            GetEffectiveKeyboardFocus() const;
        virtual InputBindingCollection      GetInputBindings() const;
        virtual bool            GetArrangeValid() const;
        virtual bool            GetEnabled() const;
        virtual bool            GetEnabledCore() const;
        virtual bool            GetFocused() const;
        virtual bool            GetHitTestVisible() const;
        virtual bool            GetInputMethodEnabled() const;
        virtual bool            GetKeyboardFocused() const;
        virtual bool            GetKeyboardFocusWithin() const;
        virtual bool            GetManipulationEnabled() const;
        virtual bool            GetMeasureValid() const;
        virtual bool            GetMouseCaptured() const;
        virtual bool            GetMouseCaptureWithin() const;
        virtual bool            GetMouseDirectlyOver() const;
        virtual bool            GetMouseOver() const;
        virtual bool            GetVisible() const;
        virtual double          GetOpacity() const;
        virtual Brush           GetOpacityMask() const;
        virtual Size            GetRenderSize() const;
        virtual Transform       GetRenderTransform() const;
        virtual Point           GetRenderTransformOrigin() const;
        virtual bool            GetSnapsToDevicePixels() const;
        virtual std::wstring    GetUID() const;
        virtual Visibility      GetVisibility() const;

        // Setters
        virtual void            SetAllowDrop(bool allowDrop);
        virtual void            SetCacheMode(const CacheMode& cacheMode);
        virtual void            SetClip(const Geometry& clip);
        virtual void            SetClipToBounds(bool clipToBounds);
        virtual void            SetEffect(const Effect& effect);
        virtual void            SetFocusable(bool focusable);
        virtual void            SetInputBindings(const InputBindingCollection& inputBindings);
        virtual void            SetEnabled(bool isEnabled);
        virtual void            SetHitTestVisible(bool hitTestVisible);
        virtual void            SetManipulationEnabled(bool isManipEnabled);
        virtual void            SetOpacity(double opacity);
        virtual void            SetOpacityMask(const Brush& opacityMask);
        virtual void            SetRenderSize(const Size& renderSize);
        virtual void            SetRenderTransform(const Transform& renderTransform);
        virtual void            SetRenderTransformOrigin(const Point& transformOrigin);
        virtual void            SetSnapsToDevicePixels(bool snapsToPixels);
        virtual void            SetUID(const std::wstring& uID);
        virtual void            SetVisibility(const Visibility& visibility);

        // Methods
        virtual void            AddHandler();
        virtual void            AddToEventRoute();
        virtual void            ApplyAnimationClock();
        virtual void            Arrange();
        virtual void            ArrangeCore();
        virtual void            BeginAnimation();
        virtual void            CaptureMouse();
        virtual void            Focus();
        virtual void            GetAnimationBaseValue();
        virtual void            GetLayoutClip();
        virtual void            GetUIParentCore();
        virtual void            HitTestCore();
        virtual void            InputHitTest();
        virtual void            InvalidateArrange();
        virtual void            InvalidateMeasure();
        virtual void            InvalidateVisual();
        virtual void            Measure();
        virtual void            MeasureCore();
        virtual void            MoveFocus();
        virtual void            PredictFocus();
        virtual void            RaiseEvent();
        virtual void            ReleaseMouseCapture();
        virtual void            RemoveHandler();
        virtual void            ShouldSerializeCommandBindings();
        virtual void            ShouldSerializeInputBindings();
        virtual void            TranslatePoint();
        virtual void            UpdateLayout();

    protected:
        virtual void            OnAccessKey();
        virtual void            OnChildDesiredSizeChanged();
        virtual void            OnCreateAutomationPeer();
        virtual void            OnDragEnter();
        virtual void            OnDragLeave();
        virtual void            OnDragOver();
        virtual void            OnDrop();
        virtual void            OnGiveFeedback();
        virtual void            OnGotFocus();
        virtual void            OnGotKeyboardFocus();
        virtual void            OnGotMouseCapture();
        virtual void            OnIsKeyboardFocusedChanged();
        virtual void            OnIsKeyboardFocusWithinChanged();
        virtual void            OnIsMouseCapturedChanged();
        virtual void            OnIsMouseCaptureWithinChanged();
        virtual void            OnIsMouseDirectlyOverChanged();
        virtual void            OnKeyDown();
        virtual void            OnKeyUp();
        virtual void            OnLostFocus();
        virtual void            OnLostKeyboardFocus();
        virtual void            OnLostMouseCapture();
        virtual void            OnManipulationBoundaryFeedback();
        virtual void            OnManipulationCompleted();
        virtual void            OnManipulationDelta();
        virtual void            OnManipulationInertiaStarting();
        virtual void            OnManipulationStarted();
        virtual void            OnManipulationStarting();
        virtual void            OnMouseDown();
        virtual void            OnMouseEnter();
        virtual void            OnMouseLeave();
        virtual void            OnMouseLeftButtonDown();
        virtual void            OnMouseLeftButtonUp();
        virtual void            OnMouseMove();
        virtual void            OnMouseRightButtonDown();
        virtual void            OnMouseRightButtonUp();
        virtual void            OnMouseUp();
        virtual void            OnMouseWheel();
        virtual void            OnPreviewDragEnter();
        virtual void            OnPreviewDragLeave();
        virtual void            OnPreviewDragOver();
        virtual void            OnPreviewDrop();
        virtual void            OnPreviewGiveFeedback();
        virtual void            OnPreviewGotKeyboardFocus();
        virtual void            OnPreviewKeyDown();
        virtual void            OnPreviewKeyUp();
        virtual void            OnPreviewLostKeyboardFocus();
        virtual void            OnPreviewMouseDown();
        virtual void            OnPreviewMouseLeftButtonDown();
        virtual void            OnPreviewMouseLeftButtonUp();
        virtual void            OnPreviewMouseMove();
        virtual void            OnPreviewMouseRightButtonDown();
        virtual void            OnPreviewMouseRightButtonUp();
        virtual void            OnPreviewMouseUp();
        virtual void            OnPreviewMouseWheel();
        virtual void            OnPreviewQueryContinueDrag();
        virtual void            OnPreviewTextInput();
        virtual void            OnQueryContinueDrag();
        virtual void            OnQueryCursor();
        virtual void            OnRender();
        virtual void            OnRenderSizeChanged();
        virtual void            OnTextInput();
        virtual void            OnVisualParentChanged();

    private:
        bool                    m_AllowDrop;
        CacheMode               m_CacheMode;                // System.Windows.Media.CacheMode
        Geometry                m_Clip;                     // System.Windows.Media.Geometry
        bool                    m_ClipToBounds;
        CommandBindingCollection  m_CommandBinding;         // System.Windows.Input.CommandBindingCollection
        Size                    m_DesiredSize;              // System.Windows.Size
        Effect                  m_Effect;                   // System.Windows.Media.Effects.Effect
        bool                    m_Focusable;
        bool                    m_AnimatedProperties;
        bool                    m_EffectiveKeyboardFocus;
        InputBindingCollection  m_InputBindings;            // System.Windows.Input.InputBindingCollection
        bool                    m_IsArrangeValid;
        bool                    m_IsEnabled;
        bool                    m_IsEnabledCore;
        bool                    m_IsFocused;
        bool                    m_IsHitTestVisible;
        bool                    m_IsInputMethodEnabled;
        bool                    m_IsKeyboardFocused;
        bool                    m_IsKeyBoardFocusWithin;
        bool                    m_IsManipulationEnabled;
        bool                    m_IsMeasureValid;
        bool                    m_IsMouseCaptured;
        bool                    m_IsMouseCaptureWithin;
        bool                    m_IsMouseDirectlyOver;
        bool                    m_IsMouseOver;
        bool                    m_IsVisible;
        double                  m_Opacity;
        Brush                   m_OpacityMask;              // System.Windows.Media.Brush
        Size                    m_RenderSize;               // System.Windows.Size
        Transform               m_RenderTransform;          // System.Windows.Media.Transform
        Point                   m_RenderTransformOrigin;    // System.Windows.Point
        bool                    m_SnapsToDevicePixels;
        std::wstring            m_UID;
        Visibility              m_Visibility;               // System.Windows.Visibility
*/
        EventDelegate                   m_DragEnterEvent;
        EventDelegate                   m_DragLeaveEvent;
        EventDelegate                   m_DragOverEvent;
        EventDelegate                   m_DropEvent;
        EventDelegate                   m_GiveFeedbackEvent;
        EventDelegate                   m_GotFocusEvent;
        EventDelegate                   m_GotKeyboardFocusEvent;
        EventDelegate                   m_GotMouseCaptureEvent;
        EventDelegate                   m_KeyDownEvent;
        EventDelegate                   m_KeyUpEvent;
        EventDelegate                   m_LostFocusEvent;
        EventDelegate                   m_LostKeyboardFocusEvent;
        EventDelegate                   m_LostMouseCaptureEvent;
        EventDelegate                   m_ManipulationBoundaryFeedbackEvent;
        EventDelegate                   m_ManipulationCompletedEvent;
        EventDelegate                   m_ManipulationDeltaEvent;
        EventDelegate                   m_ManipulationInertiaStartingEvent;
        EventDelegate                   m_ManipulationStartedEvent;
        EventDelegate                   m_ManipulationStartingEvent;
        EventDelegate                   m_MouseDownEvent;
        EventDelegate                   m_MouseEnterEvent;
        EventDelegate                   m_MouseLeaveEvent;
        EventDelegate                   m_MouseLeftButtonDownEvent;
        EventDelegate                   m_MouseLeftButtonUpEvent;
        EventDelegate                   m_MouseMoveEvent;
        EventDelegate                   m_MouseRightButtonDownEvent;
        EventDelegate                   m_MouseRightButtonUpEvent;
        EventDelegate                   m_MouseUpEvent;
        EventDelegate                   m_MouseWheelEvent;
        EventDelegate                   m_PreviewDragEnterEvent;
        EventDelegate                   m_PreviewDragLeaveEvent;
        EventDelegate                   m_PreviewDragOverEvent;
        EventDelegate                   m_PreviewDropEvent;
        EventDelegate                   m_PreviewGiveFeedbackEvent;
        EventDelegate                   m_PreviewGotKeyboardFocusEvent;
        EventDelegate                   m_PreviewKeyDownEvent;
        EventDelegate                   m_PreviewKeyUpEvent;
        EventDelegate                   m_PreviewLostKeyboardFocusEvent;
        EventDelegate                   m_PreviewMouseDownEvent;
        EventDelegate                   m_PreviewMouseLeftButtonDownEvent;
        EventDelegate                   m_PreviewMouseLeftButtonUpEvent;
        EventDelegate                   m_PreviewMouseMoveEvent;
        EventDelegate                   m_PreviewMouseRightButtonDownEvent;
        EventDelegate                   m_PreviewMouseRightButtonUpEvent;
        EventDelegate                   m_PreviewMouseUpEvent;
        EventDelegate                   m_PreviewMouseWheelEvent;
        EventDelegate                   m_PreviewQueryContinueDragEvent;
        EventDelegate                   m_PreviewQueryCursorEvent;
        EventDelegate                   m_PreviewTextInputEvent;
        EventDelegate                   m_QueryContinueDragEvent;
        EventDelegate                   m_QueryCursorEvent;
        EventDelegate                   m_TextInputEvent;
    };
}