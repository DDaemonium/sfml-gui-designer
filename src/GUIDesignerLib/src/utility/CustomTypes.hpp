#pragma once
#include "Delegate.hpp"
#include <SFML/Graphics.hpp>

namespace GUI
{

    using EventDelegateFunction = void(const sf::Event& keyPressedEvent/*, const EventArgs& args*/);
    using EventDelegate = Delegate<EventDelegateFunction>;
    using EventDelegateHandler = DelegateHandler<EventDelegateFunction>;

}