#pragma once
#include <vector>
#include <functional>
#include <algorithm>
#include <mutex>
#include "DelegateHandler.hpp"

namespace GUI
{

    template<class R, class... Args>
    class Delegate
    {
    private:
        std::vector<DelegateHandler<R, Args...>> m_functions;
        mutable std::mutex m_locker;

    public:
        Delegate<R, Args...>& operator+=(const DelegateHandler<R, Args...>& func) 
        {
            std::lock_guard lg(m_locker);
            m_functions.emplace_back(func);
            return *this;
        }
        
        Delegate<R, Args...>& operator-=(const DelegateHandler<R, Args...>& func) 
        {
            std::lock_guard lg(m_locker);
            m_functions.erase(std::remove(m_functions.begin(), m_functions.end(), func), m_functions.end());
            return *this;
        }
        
        void Clear()
        {
            std::lock_guard lg(m_locker);
            m_functions.clear();
        }

        void RemoveByIndex(size_t index)
        {
            std::lock_guard lg(m_locker);
            m_functions.erase(m_functions.begin() + index);
        }
        
        decltype(auto) operator()(Args&&... args) const
        {
            if constexpr (std::is_same<R, void()>::value)
            {
                for(const auto& func : m_functions)
                {
                    func(std::forward<Args>(args)...);
                }
            }
            else
            {
                std::vector<R> results;
                results.reserve(m_functions.size());
                for(const auto& func : m_functions)
                {
                    results.emplace_back(func(std::forward<Args>(args)...));
                }
                return results;
            }
        }
    };

}
