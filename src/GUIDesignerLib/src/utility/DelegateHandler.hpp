#pragma once

#include <memory>
#include <functional>

namespace GUI
{

     template<class R, class... Args>
     class Delegate;
     template<class R, class... Args>
     class DelegateHandler final
     {
     private:
     friend Delegate<R, Args...>;
     std::shared_ptr<std::function<R>> m_funcPtr;
     decltype(auto) operator()(Args&&... args) const
     {
          return  (*m_funcPtr)(std::forward<Args>(args)...);
     }

     public:
     explicit DelegateHandler(const std::function<R>& func):
          m_funcPtr(std::make_shared<std::function<R>>(func))
     {
     }
     
     bool operator==(const DelegateHandler<R, Args...>& delegateHandler) const
     {
          return m_funcPtr == delegateHandler.m_funcPtr;
     }
     };

}
