#include "BaseWindow.hpp"
#include "../application/Application.hpp"
#include "BaseWindowPtr.hpp"

namespace GUI
{

	BaseWindow::BaseWindow(const ControlName& windowName, size_t width, size_t height, const sf::String& title, sf::Uint32 style, const sf::ContextSettings& settings):
		sf::Window(sf::VideoMode(width, height), title, style, settings), m_windowName(windowName)
	{
		auto tmpThisWindowPtr = std::make_unique<BaseWindowPtr>(this);
		m_thisWindowPtrWrapper = tmpThisWindowPtr.get();

		auto& app = Application::Instance();
		app.AddWindow(std::move(tmpThisWindowPtr));
	}

	BaseWindow::BaseWindow():sf::Window()
	{
	}

	BaseWindow::~BaseWindow()
	{
		m_thisWindowPtrWrapper->m_isSourceAlive = false;
	}

	void BaseWindow::Open(const ControlName& windowName, size_t width, size_t height, const sf::String& title, sf::Uint32 style, const sf::ContextSettings& settings)
	{
		if (isOpen())
		{
			return;
		}

		m_windowName = windowName;
		auto tmpThisWindowPtr = std::make_unique<BaseWindowPtr>(this);
		m_thisWindowPtrWrapper = tmpThisWindowPtr.get();
		create(sf::VideoMode(width, height), title, style, settings);
		auto& app = Application::Instance();
		app.AddWindow(std::move(tmpThisWindowPtr));
	}

	void BaseWindow::NotifyControlsKeyPressedEvent(const sf::Event& keyPressedEvent)
	{
	}

	void BaseWindow::NotifyControlsResizedEvent(const sf::Event& resizedEvent)
	{
	}

	void BaseWindow::NotifyControlsLostFocusEvent(const sf::Event& lostFocusEvent)
	{
	}

	void BaseWindow::NotifyControlsGainedFocusEvent(const sf::Event& gainedFocusEvent)
	{
	}

	void BaseWindow::NotifyControlsTextEnteredEvent(const sf::Event& textEnteredEvent)
	{
	}

	void BaseWindow::NotifyControlsMouseWheelScrolledEvent(const sf::Event& mouseWheelScrolledEvent)
	{
	}

	void BaseWindow::NotifyControlsMouseButtonPressedEvent(const sf::Event& mouseButtonPressedEvent)
	{
	}

	void BaseWindow::NotifyControlsMouseMovedEvent(const sf::Event& mouseMovedEvent)
	{
	}

	void BaseWindow::NotifyControlsMouseEnteredEvent(const sf::Event& mouseEnteredEvent)
	{
	}

	void BaseWindow::NotifyControlsMouseLeftEvent(const sf::Event& mouseLeftEvent)
	{
	}

	void BaseWindow::NotifyControlsJoystickButtonPressedEvent(const sf::Event& joystickButtonPressedEvent)
	{
	}

	void BaseWindow::NotifyControlsJoystickMovedEvent(const sf::Event& joystickMovedEvent)
	{
	}

	void BaseWindow::NotifyControlsJoystickConnectedEvent(const sf::Event& joystickConnectedEvent)
	{
	}

	void BaseWindow::NotifyControlsJoystickDisconnectedEvent(const sf::Event& joystickDisconnectedEvent)
	{
	}

}
