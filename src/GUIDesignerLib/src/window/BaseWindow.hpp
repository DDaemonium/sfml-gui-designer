#pragma once

#include <map>
#include <vector>
#include <memory>
#include "../controls/Control.hpp"

namespace GUI
{

	class BaseWindowPtr;
	class Application;
	class BaseWindow : public sf::Window, public Control
	{
	protected:
		friend Application;
		using ControlName = sf::String;
		ControlName m_windowName;
		std::map<ControlName, std::shared_ptr<Control>> m_controls;
		BaseWindowPtr* m_thisWindowPtrWrapper;

	public:
		BaseWindow(const ControlName& windowName, size_t width, size_t height, const sf::String& title, sf::Uint32 style = sf::Style::Default, const sf::ContextSettings& settings = sf::ContextSettings());
		BaseWindow();
		virtual ~BaseWindow();
		virtual void Open(const ControlName& windowName, size_t width, size_t height, const sf::String& title, sf::Uint32 style = sf::Style::Default, const sf::ContextSettings& settings = sf::ContextSettings());
	private:
		virtual void NotifyControlsKeyPressedEvent(const sf::Event& keyPressedEvent);
		virtual void NotifyControlsResizedEvent(const sf::Event& resizedEvent);
		virtual void NotifyControlsLostFocusEvent(const sf::Event& lostFocusEvent);
		virtual void NotifyControlsGainedFocusEvent(const sf::Event& gainedFocusEvent);
		virtual void NotifyControlsTextEnteredEvent(const sf::Event& textEnteredEvent);
		virtual void NotifyControlsMouseWheelScrolledEvent(const sf::Event& mouseWheelScrolledEvent);
		virtual void NotifyControlsMouseButtonPressedEvent(const sf::Event& mouseButtonPressedEvent);
		virtual void NotifyControlsMouseMovedEvent(const sf::Event& mouseMovedEvent);
		virtual void NotifyControlsMouseEnteredEvent(const sf::Event& mouseEnteredEvent);
		virtual void NotifyControlsMouseLeftEvent(const sf::Event& mouseLeftEvent);
		virtual void NotifyControlsJoystickButtonPressedEvent(const sf::Event& joystickButtonPressedEvent);
		virtual void NotifyControlsJoystickMovedEvent(const sf::Event& joystickMovedEvent);
		virtual void NotifyControlsJoystickConnectedEvent(const sf::Event& joystickConnectedEvent);
		virtual void NotifyControlsJoystickDisconnectedEvent(const sf::Event& joystickDisconnectedEvent);
		
		template <class ReturnTypeByControlEvent>
		void NotifyControlsEvent(ReturnTypeByControlEvent Control::* method,const sf::Event& ControlEvent)
			{
				for(auto&[name, controlPtr] : m_controls)
				{
					std::invoke(method, controlPtr, ControlEvent);
				}
			}
	};

}
