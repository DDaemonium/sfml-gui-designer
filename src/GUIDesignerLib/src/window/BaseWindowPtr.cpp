#include "BaseWindowPtr.hpp"
#include "BaseWindow.hpp"
#include "../application/Application.hpp"

namespace GUI
{

	BaseWindowPtr::BaseWindowPtr(BaseWindow* source)
		:m_baseWindow(source),
		m_isSourceAlive(true)
	{
	}

	BaseWindow* BaseWindowPtr::GetPtr() const
	{
		return m_isSourceAlive.load() ? m_baseWindow : nullptr;
	}
	
}
