#pragma once
#include <atomic>

namespace GUI
{

	class BaseWindow;
	class Application;
	class BaseWindowPtr final {
	private:
		friend Application;
		friend BaseWindow;
		BaseWindowPtr(const BaseWindowPtr&) = delete;
		const BaseWindowPtr& operator=(const BaseWindowPtr&) = delete;
		BaseWindowPtr(BaseWindowPtr&&) = delete;
		const BaseWindowPtr& operator=(BaseWindowPtr&&) = delete;

	public:
		~BaseWindowPtr() = default;
		explicit BaseWindowPtr(BaseWindow* source);

	private:
		BaseWindow* m_baseWindow = nullptr;
		std::atomic<bool> m_isSourceAlive;
		BaseWindow* GetPtr() const;
	};

}