#include "GUIDesignerLib.hpp"

using namespace GUI;

int main()
{
	auto window = new BaseWindow;
	window->Open("1", 200, 200, "SFML works!");
	auto& app = Application::Instance();
	app.Run();
}